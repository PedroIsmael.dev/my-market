import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

/* SCREENS */
import SplashScreen from '@screens/splash/SplashScreen';
import HomeScreen   from '@screens/home/HomeScreen';
import ProductScreen from '@screens/product/ProductScreen';

/* CREATE STACK */
const Stack = createStackNavigator();

const stackOptions = {
    headerShown: false
}

function AppStack() {
    return (
        <Stack.Navigator initialRouteName="splash" screenOptions={ stackOptions }>
        
            {/* SPLASH */}
            <Stack.Screen name="splash" component={ SplashScreen }/>

            {/* HOME */}
            <Stack.Screen name="home" component={ HomeScreen }/>

            {/* PRODUCT */}
            <Stack.Screen name="product" component={ ProductScreen }/>
        </Stack.Navigator>
    )
}
 
export default function App() {
    return (
        <NavigationContainer>
            <AppStack/>
        </NavigationContainer>
    )
} 