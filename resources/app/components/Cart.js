import React, { useState } from 'react';
import { useCart } from "react-use-cart";

import { StyleSheet, SafeAreaView } from 'react-native';
import { Image, Modal, Text, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

/* STYLES */
import { tx, flex, align, justify, layout, btn } from '@styles/Main';

/* COMPONENTS */
import Button from '@components/ui/Button'

const Cart = () => {
    const {
        isEmpty,
        totalUniqueItems,
        totalItems,
        items,
        updateItemQuantity,
        removeItem,
        cartTotal
    } = useCart();
    const [modalVisible, setModalVisible] = useState(false);

    return (
        <View>  

            {/* BTN */}
            <TouchableOpacity onPress={ () => setModalVisible(true) }>
                <View style={{position: 'relative'}}>

                    {/* ICON CART */}
                    <Image style={{ width: 30, height: 30 }} resizeMode="contain" source={ require('@images/helpers/icon-cart.png') }></Image>

                    {/* COUNTER */}
                    <Text style={ styles.counter }>
                        { totalItems <= 9 ? totalItems : '9+' }
                    </Text>
                </View>
            </TouchableOpacity>

            {/* MODAL */}
            <Modal animationType="slide" visible={ modalVisible } transparent={ true }>
                <SafeAreaView style={ styles.modal }>
                    <View style={[flex.row, align.center, { height: 72, marginBottom: 20 }]}> 
                        {/* CLOSE MODAL */}
                        <TouchableOpacity onPress={ () => setModalVisible(false) }>
                            <Image style={{ width: 20, marginLeft: 20 }} resizeMode="contain" source={ require('@images/helpers/icon-times.png') }></Image>
                        </TouchableOpacity>

                        {/* TITLE */}
                        <Text style={[ styles.modalTitle, tx.s24, tx.center, tx.dark1, { zIndex: -1 }]}>
                            Mi carrito
                        </Text>
                    </View> 

                    {
                        isEmpty ?
                        <View style={{ paddingTop: 36 }}>
                            <Text style={[ tx.s24, tx.primary, tx.center, { marginBottom: 20 }]}>
                                Ups…tu carrito está vacío
                            </Text>

                            <Text style={[ tx.s14, tx.center, tx.dark1 ]}>
                                Agrega productos a él para realizar tu {'\n'} primer pedido My Market®
                            </Text>
                        </View> :

                        <ScrollView>
                            <View style={[ layout.container, { marginBottom: 100 }]}>
                                {/* LIST */}
                                {items.map((item, idx) => {
                                    return(<View style={[ styles.wrapperProduct ]} key={ idx }>

                                        <Image style={ styles.productImg } resizeMode="contain" source={{ uri: `${item.image}` }}/>
                                
                                        <View style={{ flex: 1 }}>
                                            <View style={[ flex.row, justify.between, {marginBottom: 16 }]}>
                                                <Text style={[ tx.s18, tx.dark1, { maxWidth: 200 }]} numberOfLines={ 2 }>
                                                    { item.title }
                                                </Text>
                                
                                                <TouchableOpacity onPress={() => removeItem(item.id)}>
                                                    <Image style={{ width: 20 }} resizeMode="contain" source={ require('@images/helpers/icon-trash.png') }/>
                                                </TouchableOpacity>
                                            </View>
                                
                                            <View style={[ flex.row, justify.between]}>
                                                <Text style={[ tx.s16, tx.dark1, { opacity: 0.5}]}>
                                                    Cantidad: {item.quantity}
                                                </Text>
                                
                                                <Text style={[ tx.s18, tx.dark1, { maxWidth: 260 }]}>
                                                    ${ item.price }
                                                </Text>
                                            </View>
                                        </View>
                                    </View>)
                                })}
                            </View>
                        </ScrollView>
                    }
                    
                    {/* PAY */}
                    {
                        !isEmpty &&
                        <View style={ styles.wrapperBtnPay }>
                            <Button style={[ btn.base, btn.green1 ]} title={'Continuar a pago $' + cartTotal} onPress={ () => { setModalVisible(false), navigate() }}></Button>
                        </View>
                    }
                </SafeAreaView>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    counter: {
        position: 'absolute',
        fontFamily: 'roboto-regular',
        fontSize: 12,
        color: '#ffffff',
        height: 20,
        backgroundColor: '#E6774F',
        borderRadius: 8,
        paddingHorizontal: 6,
        paddingTop: 1,
        top: -10,
        right: -12
    },
    modal: {
        backgroundColor: '#fff',
        height: '100%'
    },
    modalTitle: {
        position: 'absolute',
        left: 0,
        right: 0,
        marginHorizontal: 'auto'
    },  
    productImg: {
        width: 68,
        height: 72,
        marginRight: 20
    },
    wrapperProduct: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#ECECEC',
        paddingBottom: 20,
        marginTop: 20
    },
    wrapperBtnPay: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: '100%',
        backgroundColor: '#ffffff',
        padding: 20,
        height: 100,
        borderTopWidth: 1,
        borderColor: '#ECECEC'
    }
})

export default Cart;
