import React from 'react';
import Snackbar from 'react-native-snackbar';
import { useCart } from "react-use-cart";
import { View, Text, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Star from 'react-native-star-view';

/* STYLES */
import { btn, tx } from '@styles/Main';

/* COMPONENTS */
import Button from './ui/Button';

const Productthumbnail = ({ data, onPress }) => {
    const { addItem } = useCart();

    const handleItem = (data) => {
        addItem(data)
        Snackbar.show({
            text: 'Producto agregado al carrito',
            duration: Snackbar.LENGTH_SHORT,
        });
    }
    
    return (
        <View style={ styles.wrapper }>
            <TouchableOpacity onPress={ onPress }>
                {/* IMG */}
                <View style={[ styles.caseImg ]}>
                    <Image style={ styles.img } resizeMode="contain" source={{ uri: `${data.image}` }}></Image>
                </View>

                {/* TITLE */}
                <Text style={[ styles.title, tx.s20, tx.dark1, tx.medium, tx.hSoft, { marginBottom: 12 }]}>
                   { data.title }
                </Text>

                {/* DESCRIPTION */}
                <Text style={[ tx.s14, tx.dark1, tx.hSoft, { marginBottom: 12 }]} numberOfLines={ 2 }>
                    { data.description }
                </Text>

                {/* RATING */}
                <Star score={ data.rating.rate } style={{ marginBottom: 12 }}/>

                {/* PRICE */}
                <Text style={[ styles.title, tx.s24, tx.dark1, tx.medium, { marginBottom: 20 }]}>
                    ${ data.price }
                </Text>
            </TouchableOpacity>

            {/* ADD TO CART */}
            <Button style={[ btn.base, btn.green1 ]} title="Agregar a carrito" onPress={() => handleItem(data)}/>
        </View>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        borderWidth: 1,
        borderColor: '#eaeaea',
        padding: 20,
        marginBottom: 24
    },
    caseImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 8,
        paddingHorizontal: 32,
        marginBottom: 32
    },
    img: {
        height: 152,
        width: 160,
    },
    title: {
        maxWidth: '85%'
    }
})

export default Productthumbnail;
