import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

export default ({ title, style, onPress }) => {
    return (
        <TouchableOpacity>
            <Text onPress={ onPress } style={[ style ]}>
                { title }
            </Text>
        </TouchableOpacity>
    )
}