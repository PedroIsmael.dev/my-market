import React, { useEffect } from 'react';
import * as Animatable from 'react-native-animatable'
import { View, Text, ScrollView, StatusBar, Image } from 'react-native';

/* STYLES */
import { colors } from '@styles/Main'

function goToScreen(props, routeName ) {
    props.navigation.navigate(routeName)
}

const SplashScreen = ( props ) => {

    useEffect(() => {
        setTimeout(() => {
            goToScreen(props, 'home')
        }, 2000, this)
    }, []);

    return (
        <View animation='fadeIn' style={[{ flex: 1, justifyContent: 'center', alignItems: 'center' }, colors.bgWhite ]}>

            {/* STATUS BAR */}
            <StatusBar barStyle="dark-content" translucent backgroundColor='rgba(0,0,0,0)'/>

            <Animatable.Image
                animation='tada'
                easing='ease'
                duration={ 1800 } 
                style={{ width: 216 }} 
                resizeMode="contain" 
                source={ require('@images/layout/logo.png') }
            />

        </View>
    );
}

export default SplashScreen;
