import React, { useState, useEffect } from 'react';
import { View, Text, StatusBar, StyleSheet, Image, ImageBackground, ScrollView, ActivityIndicator } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Swiper from 'react-native-swiper'

/* STYLES */
import { align, flex, layout, tx } from '@styles/Main'

/* COMPONENTS */
import Cart from '@components/Cart'
import ProductThumbnail from '@components/ProductThumbnail';

const HomeScreen = ( props ) => {
    const [loading, setLoading] = useState(true);
    const [products, setProducts] = useState([]);

    useEffect(() => {   
        getProducts()
    }, []);

    const getProducts = async () => {
        fetch('https://fakestoreapi.com/products?limit=20') //https://fakestoreapi.com/
            .then(res => res.json())
            .then( json => {
                setProducts(json)
                setLoading(false)
            }).catch((err) => {
                console.log(err);
            })
    }

    return (
        <View style={ layout.app }>

            {/* STATUS BAR */}
            <StatusBar barStyle="dark-content" translucent backgroundColor='rgba(0,0,0,0)'/>

            <SafeAreaView style={[  layout.app ]}>

                {/* CONTENT */}
                <View style={ layout.header }>

                    {/* LOGO */}
                    <Image style={{ width: 132 }} resizeMode="contain" source={ require('@images/layout/logo.png') } />

                    {/* CART */}
                    <Cart></Cart>

                </View>

                <ScrollView>
                    {/* SWIPER */}
                    <View style={ styles.hero }>
                        <Swiper loop={ true } autoplay={ true } autoplayTimeout={ 6 }
                            dot={ <View style={[{ backgroundColor: '#ffffff' }, styles.swiperDot]}/>}
                            activeDot={ <View style={[{ backgroundColor: '#E6774F'}, styles.swiperDot]}/>}
                        >
                            <ImageBackground style={ styles.banner} source={ require('@images/screens/home/banner-1.jpg') } resizeMode="cover"/>

                            <ImageBackground style={ styles.banner} source={ require('@images/screens/home/banner-2.jpg') } resizeMode="cover"/>

                            <ImageBackground style={ styles.banner} source={ require('@images/screens/home/banner-3.jpg') } resizeMode="cover"/>
                        </Swiper>
                    </View>

                    <ScrollView style={ layout.container }>
                        <View style={[ flex.row, align.center, { marginBottom: 28 }]}>
                            <Image style={{ width: 26 }} resizeMode="contain" source={ require('@images/helpers/icon-box.png') } />
                            <Text style={[ tx.s26, tx.primary, tx.medium, { marginLeft: 12 }]}>
                                Productos
                            </Text>
                        </View>

                        {/* PRODUCTS LIST */}
                        { loading ? <ActivityIndicator size="large" color="#CFCFD5" /> :
                            products.map(( product, idx ) => {
                                return (
                                    <ProductThumbnail 
                                        key={ idx } 
                                        products={ products } 
                                        data={ product } 
                                        onPress={ () => props.navigation.navigate('product', { productData: product })} 
                                    />
                                )
                            })
                        }

                    </ScrollView>
                </ScrollView>

            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    hero: {
        width: '100%',
        height: 260,
        marginBottom: 24
    },
    banner: {
        height: 260
    },
    swiperDot: {
        width: 16,
        height: 6,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3
    }
})

export default HomeScreen;