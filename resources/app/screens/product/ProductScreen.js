import React, { useState, useEffect } from 'react';
import { useCart } from "react-use-cart";
import Snackbar from 'react-native-snackbar';
import { View, Text, StatusBar, StyleSheet, Image, ScrollView, ActivityIndicator } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Star from 'react-native-star-view';

/* STYLES */
import { align, btn, justify, layout, tx } from '@styles/Main'

/* COMPONENTS */
import Cart from '@components/Cart'
import Button from '@components/ui/Button';

const ProductScreen = (props) => {
    const [loading, setLoading] = useState(true)
    const [productId] = useState(props.route.params.productData.id);
    const [productRate] = useState(props.route.params.productData.rating.rate);
    const [product, setProduct] = useState({});
    const { addItem } = useCart();

    const getProduct = async () => {
        fetch( 'https://fakestoreapi.com/products/' + productId )
        .then( res => res.json() )
        .then( json => {
            setProduct( json )
            setLoading(false)
        })
    }

    const handleItem = (data) => {
        addItem(data)
        Snackbar.show({
            text: 'Producto agregado al carrito',
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    useEffect(() => {
        getProduct()
    }, []);

    return (
        <View style={ layout.app }>

            {/* STATUS BAR */}
            <StatusBar barStyle="dark-content" translucent backgroundColor='rgba(0,0,0,0)'/>

            <SafeAreaView style={[  layout.app ]}>

                {/* CONTENT */}
                <View style={ layout.header }>

                    <TouchableOpacity onPress={() => props.navigation.navigate('home')}>
                        <Image style={{ width: 28 }} resizeMode="contain" source={ require('@images/helpers/icon-arrow-left.png') } />
                    </TouchableOpacity>

                    {/* LOGO */}
                    <Image style={{ width: 132 }} resizeMode="contain" source={ require('@images/layout/logo.png') } />

                    {/* CART */}
                    <Cart></Cart>

                </View>

                { loading ? <ActivityIndicator size="large" color="#CFCFD5" style={{ marginTop: 40 }} /> : 
                    <ScrollView>
                        <View style={[ styles.caseImg, align.center, justify.center ]}>
                            <Image style={ styles.img } resizeMode="contain" source={{ uri: `${product.image}` }}/>
                        </View>
    
                        <View style={ layout.container }>
                            {/* TITLE */}
                            <Text style={[ tx.dark1, tx.s26, { marginBottom: 20, lineHeight: 40  }]}>
                                { product.title }
                            </Text>
    
                            {/* DESCRIPTION */}
                            <Text style={[ tx.s18, tx.dark1, { marginBottom: 12, lineHeight: 28 }]}>
                                { product.description }
                            </Text>
    
                            {/* RATING */}
                            <Star score={ productRate } style={{ marginBottom: 12 }}/>
    
                            {/* PRICE */}
                            <Text style={[ styles.title, tx.s24, tx.dark1, tx.medium, { marginBottom: 20 }]}>
                                ${ product.price }
                            </Text>
    
                            {/* ADD TO CART */}
                            <Button style={[ btn.base, btn.green1, { marginBottom: 40 }]} title="Agregar al carrito" onPress={() => handleItem(product)}/>
                        </View>
                    </ScrollView>
                }

            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    caseImg: {
        height: 340,
        marginBottom: 12
    },
    img: {
        height: '100%',
        width: '100%',
        maxWidth: 260,
        maxHeight: 260
    }
})

export default ProductScreen;
