import c from '@styles/settings/Colors.js'

export default {
    start:{
        alignItems: 'flex-start',
    },
    center:{
        alignItems: 'center',
    },
    end:{
        alignItems: 'flex-end',
    },

    selfStart: {
        alignSelf: 'flex-start'
    },
    selfCenter: {
        alignSelf: 'center'
    },
    selfEnd: {
        alignSelf: 'flex-end'
    }
}