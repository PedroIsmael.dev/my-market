import c from '@styles/settings/Colors.js'

export default {
    start: {
        justifyContent: 'flex-start'
    },
    center: {
        justifyContent: 'center',
    },
    end: { 
        justifyContent: 'flex-end'
    },
    around: {
        justifyContent: 'space-around',
    },
    between: {
        justifyContent: 'space-between'
    },
}