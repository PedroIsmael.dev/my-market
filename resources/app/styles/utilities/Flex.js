import c from '@styles/settings/Colors.js'

export default {
    column: {
        flexDirection: 'column'
    },
    row: {
        flexDirection: 'row'
    }
}