import c from '@styles/settings/Colors.js'

export default {
    s10: {
        fontSize: 10
    },
    s12: {
        fontSize: 12
    },
    s14: {
        fontSize: 14
    },
    s16: {
        fontSize: 16
    },
    s18: {
        fontSize: 18
    },
    s20: {
        fontSize: 20
    },
    s22: {
        fontSize: 22
    },
    s24: {
        fontSize: 24
    },
    s26: {
        fontSize: 26
    },
    s28: {
        fontSize: 28
    },
    s30: {
        fontSize: 30
    },
    s32: {
        fontSize: 32
    },
    s34: {
        fontSize: 34
    },
    s36: {
        fontSize: 36
    },
    s38: {
        fontSize: 38
    },
    s40: {
        fontSize: 40
    },
    s42: {
        fontSize: 42
    },
    s44: {
        fontSize: 44
    },
    s46: {
        fontSize: 46
    },
    s48: {
        fontSize: 48
    },
    s50: {
        fontSize: 50
    },

    white: {
        color: '#fff'
    },
    dark1: {
        color: '#000'
    },
    primary: {
        color: c.primary
    },

    uppercase: {
        textTransform: 'uppercase'
    },

    left: {
        textAlign: 'left'
    },
    center: {
        textAlign: 'center'
    },
    right: {
        textAlign: 'right'
    },

    underline: {
        textDecorationLine: 'underline',
    },

    medium: {
        fontFamily: 'roboto-medium'
    },
    bold: {
        fontFamily: 'roboto-bold'
    },

    hSoft: {
        lineHeight: 22
    },
}