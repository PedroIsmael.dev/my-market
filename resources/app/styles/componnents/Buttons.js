import c from '../settings/Colors'

export default {
    base: {
        height: 48,
        width: '100%',
        fontFamily: 'roboto-bold',
        fontSize: 16,
        textAlign: 'center',
        paddingTop: 13,
        backgroundColor: '#ECECEC',
        color: '#000'
    },
    dark1: {
        color: '#fff',
        backgroundColor: c.dark1
    },
    green1: {
        color: '#fff',
        borderWidth: 1,
        backgroundColor: '#58ba4e',
        borderColor: '#51b074'
    }
}