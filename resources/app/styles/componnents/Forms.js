import c from '../settings/Colors'

export default {
    group:{
        marginBottom: 16
    },
    label: {
        color: '#000',
        fontFamily: 'roboto-medium'
    },
    input: {
        fontSize: 16,
        height: 48,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 6,
        paddingHorizontal: 8
    }
}