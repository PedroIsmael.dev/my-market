import { StyleSheet } from 'react-native'

/* UTILITIES */
import alignUtilities     from '@styles/utilities/Align'
import colorsUtilities    from '@styles/utilities/Colors'
import flexUtilities      from '@styles/utilities/Flex'
import justifyUtilities   from '@styles/utilities/Justify'
import txUtilities        from '@styles/utilities/Typography'

/* COMPONENTS */
import formsStyles from './componnents/Forms'
import btnStyles   from './componnents/Buttons'

const layout = StyleSheet.create({
    app: {
        backgroundColor: '#fff',
        minHeight: '100%',
        flex: 1
    },  
    container: {
        paddingRight: 20,
        paddingLeft: 20
    },
    header: {
        height: 72, 
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#eaeaea'
    },
    hr: {
        borderBottomColor: '#000', 
        borderBottomWidth: 1,
    },
    hrColor: {
        height: 1,
        width: '100%'
    }
});

/* CREATE STYLE SHEET - UTILITITES */
const align     = StyleSheet.create( alignUtilities )
const colors    = StyleSheet.create( colorsUtilities )
const flex      = StyleSheet.create( flexUtilities )
const justify   = StyleSheet.create( justifyUtilities )
const tx        = StyleSheet.create( txUtilities )

/* CREATE STYLE SHEET - COMPONENTS */
const form   = StyleSheet.create( formsStyles )
const btn    = StyleSheet.create( btnStyles )

export { layout }
export { align, flex, colors, justify, tx } 
export { form, btn }
