/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import AppNavigation from '@navigation/AppNavigation';
 import { CartProvider } from "react-use-cart";

 
 const App = () => {
   return (
      <CartProvider>
         <AppNavigation/>
      </CartProvider>
   )
 }
 
 export default App;